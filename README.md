# Predictive Estimator #

Polynomial Curve Fitting
(Linear Regression)

## Run Program

To run the program issue the following at a Linux shell:

```bash
$ Rscript no-reg-train.R	# no regulaization
$ Rscript with-reg-train.R	# with regulaization
```

You need to have R, Rscript, and ggplot2 installed.

For analysis, results, and discussion, look at REPORT.pdf.